# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define t = Character("Tairitsu")
define p = Character("Protagonis")
#Custom Transition
define flash = Fade(0.1, 0.0, 0.5, color="#fff")
define flash2 = Fade(0.1, 0.0, 3.0, color="#fff")
define fadehold = Fade(0.5, 1.0, 0.5)

#Image
image b = "road.jpg"
image c = "classroom.jpg"
image ta = "Carmellia05.png"
image ta2 = "Carmellia03.png"
image tr = "Black.png"
image s = "speech.png"
image s2 = "speech2.png"
image s3 = "speech3.png"
# The game starts here.

label start:
    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene b with fade
    play music "Emotion.mp3" fadeout 1.0 fadein 1.0

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.
    

    # These display lines of dialogue.
label intro:
    show ta
    with dissolve
    t "(Hmn....)"
    "(Ia menatapmu)"
    t "(.....?)"
    p "(......)"
    t "Apakah ada yang bisa ku bantu?"
    p "(...)"
    t "Hmmmn, Kau memintaku untuk mengajarimu tentang suatu pelajaran?"
    p "(.....!)"
    "(Wanita itu tersenyum kecil.)"
    t "Baiklah apa yang ingin kau pelajari anak muda?"
    p "(.....!)"
    t "Materi direct - indirect speech? Bahasa inggris rupanya."
    t "Mengapa kau ingin belajar hal ini?"
    p "(.....!)"
    t "Hanya ingin belajar? Ahahaha niat yang bagus."
    t "Baiklah, aku akan ajari semampuku, tetapi kau harus tetap belajar mandiri dan jangan malas ya!"
    t "Baiklah ayo kita mulai."
    hide ta
    with dissolve
label study:
    play music "song2.mp3" fadeout 1.0 fadein 1.0
    scene c with fade
    show ta
    with dissolve
    t "Hmm...Kita mulai dari mana ya...?"
    t "Oh iya apa yang kau ketahui tentang direct - indirect speech?"
    menu:
        "Saya tidak tahu apa itu direct - indirect speech":
             t "Hmn? Biar aku jelaskan."
             t "Sebenarnya ada dua materi yang kau ajukan kepadaku."
             t "Pertama adalah direct speech dan yang kedua adalah indirect speech."
             t "Simplenya seperti ini, kalian tau definisi arti kata direct dan indirect bukan?"
             t "Ya! Direct artinya langsung dan Indirect artinya tidak langsung."
             t "Lalu apa itu speech?"
             t "Speech artinya berpidato, namun arti ini bukan pidato yang biasa dilakukan orang - orang di acara resmi."
             t "Coba kamu berpikir? Apa tujuan kita berpidato?"
             p "(.....?)"
             t "Untuk menyampaikan suatu pesan kepada orang lain, benar?"
             p "(.....!)"
             "(Dengan senyum kecilnya ia menertawakanmu.)"
             t "Hihihi, kau lucu juga ya."
             p "(.....!)"
             t "Ahahaha, tidak apa - apa..."
             t "Mari kita lanjutkan."
             t "Lalu coba kamu gabungkan dua definisi yang telah kita temukan tadi."
             t "Direct speech berarti menyampaikan suatu pesan secara langsung."
             t "Indirect speech berarti menyampaikan pesan melalui suatu perantara, dengan kata lain tidak langsung."
             t "Apakah kau mengerti?"
             p "(.....!)"

        "Direct - indirect speech adalah...":
             "Kau menjelaskannya dengan baik."
    t "Ok karena kau sudah mengerti mari kita lanjutkan mengenai cara penggunaanya."
    t "Aku akan ajarkan konsep mudahnya saja, jika kamu tertarik lebih lanjut cobalah mencarinya dengan semangat yang kau punya."
    t "Sudah siap?"
    p "(.....!)"
    t "Sekarang kita akan masuk ke dalam struktur baku direct - indirect speech."
    hide ta
    with dissolve
    show ta2 at right
    with dissolve
    show s at topleft behind ta2
    with dissolve
    t "Apakah kamu bisa melihatnya? Ini adalah tabel dasar yang kamu harus ketahui untuk mempejari materi ini."
    p "(.....!)"
    t "Hmmmn, tidak usah terlalu takut."
    t "Aku tidak menyuruhmu untuk menghafal ini kan? Ahahaha..."
    t "Tabel di sebelahku ini akan membantu kita dalam mempelajari direct - indirect speech."
    t "kolom sebelah kiri adalah bentuk pernyataan direct speech, artinya kalimat itu diutarakan langsung oleh orang yang mengucapkannya."
    t "lalu kolom sebelah kanan adalah bentuk pernyataan indirect speech, artinya kalimat itu diutarakan oleh pihak lain (Disampaikan)."
    t "Coba kamu perhatikan kalimat - kalimat itu, kalau kamu teliti maka kamu akan melihat suatu pola yang akan membantumu memahami ini."
    p "(.....?)"
    t "Kau belum menemukannya? baiklah aku beri petunjuk, ada dua pola dalam tabel ini."
    p "(.....!)"
    t "Ah, kau sudah menemukannya."
    hide s
    with dissolve
    t "Inilah pola yang pertama."
    show s2 at topleft behind ta2
    with dissolve
    t "Lihat tanda yang kuberi warna?"
    t "tanda - tanda itu menandakan adanya perubahan tenses setiap kita membuat suatu direct speech menjadi indirect speech."
    t "Misal jika ada kata eat di kalimat present maka kamu harus menggantinya dengan ate."
    p "(.....?)"
    t "Huuuh, akan kuberikan secara berurutan. tolong kamu lihat pola kedua ini."
    hide s2
    with dissolve
    show s3 at topleft behind ta2
    with dissolve
    t "Dalam direct speech atau indirect speech kita mengetahui bahwa lawan bicara kita sedang mengatakan suatu hal. benar?"
    p "(.....!)"
    t "Nah pertanyaanku, kapan orang itu menyampaikan pesannya kepadamu?"
    p "(.....!)"
    t "Tepat, bisa kapan saja, bisa kemarin ataupun hari ini."
    t  "Ingat sesuatu? Tenses apa yang menyatakan kejadian yang terjadi sekarang (atau hari ini.)?"
    menu:
        "Present tenses?":
             t "Tepat!"

        "Past tenses?":
             t "Kurang tepat, past tenses digunakan pada masa lampau, jawabannya dalah present tenses."
    t "Lalu apa bahasa yang baik jika kita ingin menyampaikan sesuatu pesan tersebut dalam bahasa inggris?"
    p "(.....!)"
    t "Ya, {i}say, explain, ask, tell,{i} dan lain sebagainya."
    t "Jika memang itu present maka cukup mengikuti kaidah present tenses."
    t "contoh..."
    t "I say <Masukan kalimatmu.>, she says <Masukan kalimatmu.>, dan lain - lain."
    t "Jika past tenses? cukup ubah kalimat {i}say, explain, ask, tell,{i} ke bentuk lampau, seperti {i}she explained, he asked, it said, i told{i} dan sebagainya."
    p "(.....!)"
    t "Berikutnya, kamu sadar di tabel itu ada kotak lain yang kuberi warna berbeda?"
    t "Tanda - tanda itu menunjukan perubahan subject jika kita mengubah direct speech menjadi indirect speech."
    t "Contohnya yang baris pertama."
    t "{i}Lelaki itu menjelaskan \"aku tidak penah makan daging\"{i}."
    t "Sekarang coba kau sampaikan pesan itu kepada temanmu."
    t "Pasti kau akan mengatakan {i}Lelaki itu menjelaskan bahwa dia tidak pernah makan daging{i}."
    t "\"dia\" di sini siapa ya?"
    p "(.....!)"
    t "Yup, sang lelaki yang baru menyampaikan pesannya kepadamu."
    t "Sekarang lihat tabel, paham kan maksudku mengapa subjectnya berubah?"
    p "(.....!)"
    t "Kembali ke pola pertama."
    hide s3
    with dissolve
    show s2 at topleft behind ta2
    with dissolve
    t "Pola di sini adalah tenses, aku tidak bisa menjelaskannya secara spesifik namun dirimu tidak perlu menghafal rumus tenses."
    t "lihatlah polanya..."
    t "jika present menjadi past."
    t "jika continuous menjadi past continuous seperti..."
    t "{i}i \"am waiting\"{i} menjadi {i}\"was waiting\"{i} atau {i}They \"are\" sleeping{i} menjadi {i} they \"were\" sleeping{i}."
    t "Lalu {i}Have{i} menjadi {i}had{i}, kurang lebih seperti itu."
    t "materi ini mudah jika kamu memang belajar untuk mengerti bukan untuk mengghafal."
    t "Mengerti?"
    p "(.....!)"
    hide s2
    with dissolve
    hide ta2
    with dissolve
    show ta
    with dissolve
    t "Senang bisa membantumu, oh iya aku punya saran."
    t "Jika kamu masih kesulitan mengenai materi ini, maka kamu cukup mempejari kembali tenses - tenses yang digunakan."
    t "jikalah kamu berhasil, maka aku yakin materi sanggup dirimu kuasai."
    menu:
        "Terima Kasih banyak!":
           t "Sama -sama, ahahaha..."

        "Kau adalah guru yang terbaik!":
           t "Kau terlalu memuji."
    t "Kalau dirimu butuh bantuan kontak saja aku."
    "(Wanita itu memberikan kontaknya kepadamu.)"
    t "Sepertinya diriku terlalu bersemangat, ahahaha..."
    t "Sampai jumpa lagi."
    p "(.....!)"
    hide ta
    with fadehold
    scene b with fade
    ""
    ""
    ""
    ""
    # This ends the game.

    return
